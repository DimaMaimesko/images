<?php

namespace frontend\modules\qqq\controllers;

use yii\web\Controller;

/**
 * Default controller for the `qqq` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
