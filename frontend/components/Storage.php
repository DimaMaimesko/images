<?php

namespace frontend\components;

use Yii;
use yii\base\Component;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * File storage compoment
 *
 * @author admin
 */
class Storage extends Component implements StorageInterface
{

    private $fileName;
    private $path_small;
    private $userID;
    /**
     * 
     * @param string $filename
     * @return string
     */
    public function getFile(string $filename)
    {
       return Yii::$app->params['storageUriResized'].$filename;
    }
    /**
     * Save given UploadedFile instance to disk
     * @param UploadedFile $file
     * @return string|null
     */
    public function saveUploadedFile(UploadedFile $file, $userID)
    {
        $this->userID = $userID;
        $path = $this->preparePath($file); 
        $file->saveAs($path['path_small']);
        return [
            'fName' => $this->userID.'/'.$this->fileName,
            'pName' => $file->name,
            'pSize' => $file->size
            ];
    }
     /**
     * Prepare path to save uploaded file
     * @param UploadedFile $file
     * @return string|null
     */
    protected function preparePath(UploadedFile $file)
    {
        $this->fileName = $this->getFileName($file);  
        //     0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg
        $this->path_small = $this->getStoragePath() . $this->fileName;  
        //     /var/www/project/frontend/web/uploads/resized/23/0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg
        $this->path_small = FileHelper::normalizePath($this->path_small);
        
        if (FileHelper::createDirectory(dirname($this->path_small))) {
            return [
                'path_small' => $this->path_small,
                ];
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    protected function getFilename(UploadedFile $file)
    {
        // $file->tempname   -   /tmp/qio93kf
        
        $hash = sha1_file($file->tempName); // 0ca9277f91e40054767f69afeb0426711ca0fddd
        
        $name = substr_replace($hash, '/', 2, 0);
        $name = substr_replace($name, '/', 5, 0);  // 0c/a9/277f91e40054767f69afeb0426711ca0fddd
        return $name . '.' . $file->extension;  // 0c/a9/277f91e40054767f69afeb0426711ca0fddd.jpg
    }

    /**
     * @return string
     */
    protected function getStoragePath()
    {
       return Yii::getAlias(Yii::$app->params['storagePathResized'].$this->userID.'/');//path/to/frontend/web/uploads/resized/23
    }
    protected function getStoragePath2()
    {
       return Yii::getAlias(Yii::$app->params['storagePathResized'].$this->userID);//path/to/frontend/web/uploads/resized/23
    }

    /**
     * @param string $filename
     * @return boolean
     */
    public function deleteFile(string $filename, $userId)
    {
        //print_r($filename);die;//   /39/31/d3/858c09bda6a514b66456a51aa473ebfb3f68.jpg
       //print_r(substr($filename, 3,2));die;
        $file = $this->getStoragePath2().$filename;
        //print_r($file);die;//   /app/frontend/web/uploads/resized//39/31/d3/858c09bda6a514b66456a51aa473ebfb3f68.jpg
        $dirToFile = $this->getStoragePath2();
        //print_r($dirToFile);die; //  /app/frontend/web/uploads/resized/
        $dirToFile = $dirToFile.$userId.substr($filename, 2,3);
        $dirToFile = FileHelper::normalizePath($dirToFile);
        //print_r($dirToFile);die; //  /app/frontend/web/uploads/resized/39/5b
        FileHelper::removeDirectory($dirToFile,true);
        if (file_exists($file)) {
            return unlink($file);
        }
        return true;
    }
}
